import { registerBlockType } from '@wordpress/blocks';
import { TestimonialListEdit } from './edit';
 
registerBlockType( 'stardust/testimonial-list', {
    apiVersion: 2,
    title: 'Testimonial List',
    icon: 'universal-access-alt',
    category: 'widgets',
    example: {},
    edit: TestimonialListEdit,
} );
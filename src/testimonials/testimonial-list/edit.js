import { useSelect } from '@wordpress/data';
import { useBlockProps } from '@wordpress/block-editor';

export const TestimonialListEdit = () => {
    const blockProps = useBlockProps();
    const posts = useSelect( ( select ) => {
        return select( 'core' ).getEntityRecords( 'postType', 'star-testimonials', {
            'status': 'publish',
            'per_page': 5,
        } );
    }, [] );

    return (
        <div { ...blockProps }>
            { ! posts && 'Loading' }
            { posts && posts.length === 0 && 'No Posts' }
            { posts && posts.length > 0 && posts.map((post) => (
                <a href={ post.link }>
                    { post.title.rendered }
                </a>
            )) }
        </div>
    );
};

<div>
    <?php
    global $star_language;
    $query = new WP_Query( array(
        'post_type' => 'star-testimonials',
        'post_status' => 'publish',
        'meta_key' => 'language',
        'meta_value' => $star_language
    ) );
    if ( $query->have_posts() ):
        while ( $query->have_posts() ): $query->the_post();
            ?>
            <div class="stardust-testimonial">
                <?php the_post_thumbnail(); ?>
                <div class="stardust-testimonial-body">
                    <h3><?php the_title(); ?></h3>
                    <?php the_content();  ?>
                </div>
            </div>
            <?php
        endwhile;
        wp_reset_postdata();
    endif;
    ?>
</div>

<?php
$occupation = get_post_meta( $post->ID, 'stardust_testimonials_occupation', true );
$company    = get_post_meta( $post->ID, 'stardust_testimonials_company', true );
$user_url   = get_post_meta( $post->ID, 'stardust_testimonials_user_url', true );
?>

<table class="form-table stardust-plugin-metabox">
    <input type="hidden" name="stardust_testimonials_nonce" value="<?php echo wp_create_nonce( 'stardust_testimonials_nonce' ); ?>">
    <tr>
        <th>
            <label for="stardust_testimonials_occupation"><?php esc_html_e( 'User occupation', 'stardust-types' ); ?></label>
        </th>
        <td>
            <input
                type="text"
                name="stardust_testimonials_occupation"
                id="stardust_testimonials_occupation"
                class="regular-text occupation"
                value="<?php echo isset( $occupation ) ? esc_html( $occupation ) : '' ?>"
            >
        </td>
    </tr>
    <tr>
        <th>
            <label for="stardust_testimonials_company"><?php esc_html_e( 'User company', 'stardust-types' ); ?></label>
        </th>
        <td>
            <input
                type="text"
                name="stardust_testimonials_company"
                id="stardust_testimonials_company"
                class="regular-text company"
                value="<?php echo isset( $company ) ? esc_html( $company ) : '' ?>"
            >
        </td>
    </tr>
    <tr>
        <th>
            <label for="stardust_testimonials_user_url"><?php esc_html_e( 'User URL', 'stardust-types' ); ?></label>
        </th>
        <td>
            <input
                type="url"
                name="stardust_testimonials_user_url"
                id="stardust_testimonials_user_url"
                class="regular-text user-url"
                value="<?php echo isset( $user_url ) ? esc_url( $user_url ) : '' ?>"
            >
        </td>
    </tr>
</table>

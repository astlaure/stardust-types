<?php

if (!class_exists('StardustTypesGutenbergBlocks')) {
    class StardustTypesGutenbergBlocks {
        function __construct() {
            add_action('init', array($this, 'register_scripts'));
            add_action('enqueue_block_assets', array($this, 'enqueue_block_assets'));
            add_action('enqueue_block_editor_assets', array($this, 'enqueue_block_editor_assets'));
        }

        function register_scripts() {
            // automatically load dependencies and version
            $asset_file = include(plugin_dir_path(__FILE__) . '../build/index.asset.php');

            wp_register_script(
                'stardust-types-gutenberg',
                plugins_url('build/index.js', __FILE__),
                $asset_file['dependencies'],
                $asset_file['version']
            );

            $this->register_dynamic_block('stardust/testimonial-list', 'testimonials_list_callback');
        }

        function enqueue_block_assets()
        {
            wp_enqueue_style(
                'stardust-types-gutenberg',
                plugins_url('assets/css/style.css', __FILE__),
                array(),
                '1.0.0'
            );
        }

        function enqueue_block_editor_assets()
        {
            wp_enqueue_style(
                'stardust-types-editor',
                plugins_url('assets/css/editor.css', __FILE__),
                array(),
                '1.0.0'
            );
        }

        function register_block($name)
        {
            register_block_type($name, array(
                'api_version' => 2,
                'editor_script' => 'stardust-types-gutenberg',
                // 'style' => 'stardust-blocks'
            ));
        }

        function register_dynamic_block($name, $callback)
        {
            register_block_type($name, array(
                'api_version' => 2,
                'editor_script' => 'stardust-types-gutenberg',
                'render_callback' => array($this, $callback)
            ));
        }

        function register_block_style($block, $name, $label)
        {
            register_block_style($block, array(
                'name' => $name,
                'label' => __($label, 'stardust-types')
            ));
        }

        // CALLBACKS
        function testimonials_list_callback($attributes, $content)
        {
            ob_start();
            require __DIR__ . '/../views/testimonials-grid.php';
            return ob_get_clean();
        }
    }
}

<?php

if ( !class_exists( 'StardustTypesTestimonialsGrid' ) ) {
    class StardustTypesTestimonialsGrid {
        function __construct() {
            add_shortcode( 'stardust_testimonials_grid', array( $this, 'add_shortcode' ) );
        }

        public function add_shortcode( $atts = array(), $content = null, $tag = '' ) {
            $atts = array_change_key_case( (array) $atts, CASE_LOWER );

            ob_start();
            require __DIR__ . '/../views/testimonials-grid.php';
            return ob_get_clean();
        }
    }
}

<?php
/*
Plugin Name: Stardust Types
Description: Types Plugin for the Stardust Ecosystem. Provide Testimonials.
Version: 1.0.0
Author Name: Alexandre St-Laurent
Author URI: https://github.com/astlaure
*/

if ( !defined( 'ABSPATH' ) ) {
    exit;
}

if ( !class_exists( 'StardustTypes' ) ) {
    class StardustTypes {
        function __construct() {
            require_once __DIR__ . '/classes/stardust-testimonials.php';
            require_once __DIR__ . '/classes/gutenberg-blocks.php';
            require_once __DIR__ . '/shortcodes/stardust-testimonials-grid.php';

            new StardustTypesTestimonials();
            new StardustTypesGutenbergBlocks();
            new StardustTypesTestimonialsGrid();
        }

        static public function activate() {
            require_once( ABSPATH . '/wp-admin/includes/plugin.php' );

            if ( !is_plugin_active( 'stardust-core/stardust-core.php' ) ) {
                ?>
                <div class="notice notice-error" >
                    <p>Please enable Stardust Core before activating Stardust I18n</p>
                </div>
                <?php
                @trigger_error(__( 'Please enable Stardust Core before activating Stardust I18n', 'stardust-i18n' ), E_USER_ERROR);                
            }
            
            update_option( 'rewrite_rules', '' );
        }

        static public function deactivate() {
            flush_rewrite_rules();
        }
        
        static public function uninstall() {}
    }
}

if ( class_exists( 'StardustTypes' ) ) {
    register_activation_hook( __FILE__, array( 'StardustTypes', 'activate' ) );
	register_deactivation_hook( __FILE__, array( 'StardustTypes', 'deactivate' ) );
	register_uninstall_hook( __FILE__, array( 'StardustTypes', 'uninstall' ) );

    new StardustTypes();
}
